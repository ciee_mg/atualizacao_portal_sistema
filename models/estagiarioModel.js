const db = require('../config/db_mysql');
const type = require('sequelize');

let estagiarioModel = db.define('estagiario', {
    id: {
        type: type.INTEGER(15),
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    estudante_id: {
       type: type.INTEGER(11),
       allowNull: false
    },
    empresa_id: {
        type: type.INTEGER(11),
        allowNull: false
    },
    cpf: {
        type: type.STRING(14),
        allowNull: true
    },
    senha: {
        type: type.STRING(255),
        allowNull: true
    },
    ativo: {
        type: type.INTEGER(8),
        allowNull: true
    },
    vigenciainicio: {
        type: type.DATE,
        allowNull: true
    },
    vigenciafim: {
        type: type.DATE,
        allowNull: true
    },
    agencia: {
        type: type.STRING(45),
        allowNull: true
    },
    digito_agencia: {
        type: type.STRING(2),
        allowNull: true
    },
    banco: {
        type: type.STRING(6),
        allowNull: true
    },
    valor_bolsa: {
        type: type.DECIMAL(10,2),
        allowNull: true
    },
    valor_transporte: {
        type: type.DECIMAL(10,2),
        allowNull: true
    },
    sequencial: {
        type: type.STRING(3),
        allowNull: true
    },
    conta: {
        type: type.STRING(45),
        allowNull: true
    },
    nome: {
        type: type.STRING(255),
        allowNull: true
    },
    motivorescisao: {
        type: type.STRING(2),
        allowNull: true
    },
    outro_motivo: {
        type: type.TEXT,
        allowNull: true
    },
    vigenciatermino: {
        type: type.DATE,
        allowNull: true
    },
    dt_rescisao: {
        type: type.DATE,
        defaultValue: '0000-00-00',
        allowNull: true
    },
    dt_bolsa: {
        type: type.DATE,
        allowNull: true
    },
    dt_prorrogacao: {
        type: type.DATE,
        allowNull: true
    },
    horas_mensal: {
        type: type.STRING(3),
        allowNull: true
    },
    email: {
        type: type.STRING(255),
        allowNull: true
    },
    previsao_ano: {
        type: type.INTEGER(4),
        allowNull: true
    },
    previsao_mes: {
        type: type.TINYINT(2),
        allowNull: true
    },
    id_ciee15_s0: {
        type: type.STRING(20),
        allowNull: true
    },
    regiao: {
        type: type.STRING(3),
        allowNull: true
    },
    local_estagio: {
        type: type.STRING(3),
        allowNull: true
    },
    digito: {
        type: type.STRING(2),
        allowNull: true
    },
    operacao: {
        type: type.STRING(11),
        allowNull: true
    },
    operacao_cef: {
        type: type.STRING(4),
        allowNull: true
    },
    dt_conta: {
        type: type.DATE,
        allowNull: true
    },
    responsavel: {
        type: type.STRING(250),
        allowNull: true
    },
    email_responsavel: {
        type: type.STRING(250),
        allowNull: true
    },
    telefone: {
        type: type.STRING(14),
        allowNull: true
    },
    cargo: {
        type: type.STRING(100),
        allowNull: true
    },
    email_empresa: {
        type: type.STRING(255),
        allowNull: true
    }
});

module.exports = estagiarioModel;