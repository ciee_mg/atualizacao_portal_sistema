//modelo de dados 
const db = require('../config/db_mysql');
const type = require('sequelize');

let empresaModel = db.define('empresa', {
    id: {
        type: type.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    razaosocial: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    cnpj: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true,
    },
    logradouro: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: true,        
    },
    numero: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    complemento: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    cep: {
        type: type.STRING(10),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    telefone: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    nomecontato: {
        type: type.STRING(100),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    email: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    importado: {
        type: type.TINYINT(4),
        allowNull: false
    },
    senha: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    cidade: {
        type: type.TEXT,
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    bairro: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    tipoPagamento: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    tipoRI: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    RI: {
        type: type.DECIMAL(10,2),
        allowNull: true
    },
    orgao_publico: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    convenio: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    contrato: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    dt_vencimento: {
        type: type.STRING(2),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    regiao: {
        type: type.STRING(3),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    dt_validade: {
        type: type.DATE,
        allowNull: false
    },
    avatar: {
        type: type.TEXT,
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    enviar: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    cargo: {
        type: type.STRING(45),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    dt_atualizacao: {
        type: type.DATE,
        allowNull: true
    },
    nomecontato2: {
        type: type.STRING(100),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    cargo2: {
        type: type.STRING(100),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    email1: {
        type: type.STRING(255),
        collate: 'latin1_swedish_ci',
        allowNull: false
    },
    trocar_senha: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    TIPOPROGRAMA: {
        type: type.STRING(1),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    ac_fatura: {
        type: type.STRING(100),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    banco_pagamento: {
        type: type.STRING(3),
        collate: 'latin1_swedish_ci',
        allowNull: true
    },
    valor_desconto: {
        type: type.DECIMAL(10,2),
        allowNull: true
    },
    bloqueia_nova_folha: {
        type: type.TINYINT(1),
        allowNull: false
    },
    fechamento_folha_dias: {
        type: type.INTEGER(2),
        allowNull: false
    },
    dt_ultimo_email_erro_fechamento: {
        type: type.DATE,
        allowNull: true
    }
})
module.exports = empresaModel;