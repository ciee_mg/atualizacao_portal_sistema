require('dotenv/config');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.db_name, process.env.db_user, process.env.db_pass, {
    host:process.env.db_host,
    port:process.env.db_port,
    dialect:'mysql',
    define:{
        timestamps: false,
        paranoid: true,
        freezeTableName: true,
    }
})

module.exports = sequelize;