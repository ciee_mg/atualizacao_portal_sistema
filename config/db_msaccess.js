require('dotenv/config');
const ado = require('node-adodb');

let conexao = ado.open(process.env.db_msacces);

module.exports = conexao;