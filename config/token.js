require('dotenv/config');

function validarCabecalho(header){
    const token = process.env.token;
    let valido = false;
    if((header) && (header===token)){
        valido = true;
    }

    return valido;
}

module.exports = validarCabecalho;