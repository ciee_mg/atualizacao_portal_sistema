require('dotenv/config')
const restify = require('restify');
const cors = require('cors');
const port = process.env.Port;
const server = restify.createServer();

server.use(restify.plugins.bodyParser({
    mapParams:true,
    mapFiles:false,
    overrideParams:false,
}));

server.use(cors())


module.exports = {server, port};
