const model = require('../models/estagiarioModel');
const msAccess = require('../functions/funcMsAccess');

function buscarEstagiarios(skip, limit){
    return new Promise((resolve, reject) => {
        model.findAll({
            offset:skip,limit:limit
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao buscar estudantes: ${err}`);
            reject(err)
        })
    });
}

function insereEstagiario(data){
    return new Promise((resolve, reject) => {
        model.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao inserir estagiário: ${err}`);
            reject(err)
        })
    });
}

function atualizaEstagiario(data){
    return new Promise((resolve, reject) => {
        model.update(data, {
            where: { id:data.id }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao atualizar os dados do estagiário: ${err}`);
            reject(err);
        })
    });
}

function deleteEstagiario(data){
    return new Promise((resolve, reject) => {
        model.destroy({
            where: { id:data.id }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao deletar dados do estagiario: ${err}`);
        })
    });
}

function numeroEstagiarios(data){
    return new Promise((resolve, reject) => {
        model.count({
            where: {
                motivorescisao: ['', null],
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao contar estagiarios: ${err}`);
            reject(err)
        })
    })
}

function nEst(){
    return new Promise((resolve, reject) => {
        msAccess.estagiariosGeral().then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao buscar estagiarios: ${err}`);
            reject(err)
        })
    });
}

module.exports = { buscarEstagiarios, insereEstagiario, atualizaEstagiario, deleteEstagiario, numeroEstagiarios, nEst }