const model = require('../models/empresaModel');

function buscarEmpresa(skip, limit){
    return new Promise((resolve, reject) => {
        model.findAll({
            offset:skip,limit:limit
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao buscar empresas: ${err}`);
            reject(err);
        })
    });
}

function insereEmpresa(data){
    return new Promise((resolve, reject) => {
        model.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao inserir empresa: ${err}`);
            reject(err);
        })
    });
}

function atualizaEmpresa(data){
    return new Promise((resolve, reject) => {
        model.update(data, {
            where: { id:data.id }
        }).then(res => {
            resolve(res);
        }).catch(err =>{
            console.log(`erro ao atualizar empresa: ${err}`);
            reject(err);
        })
    })
}

function deletaEmpresa(data){
    model.destroy({
        where: { id:data.id }
    }).then(res => {
        resolve(res);
    }).catch(err => {
        console.log(`erro ao excluir registro: ${err}`);
        reject(err);
    })
}


module.exports = {buscarEmpresa, insereEmpresa, atualizaEmpresa, deletaEmpresa}