const model = require('../models/escolasModel');

function buscarEscolas(skip, limit){
    return new Promise((resolve, reject) => {
        model.findAll({
            offset:skip,limit:limit
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao buscar escolas: ${err};`);
            reject(err);
        })
    });
}

function insertEscola(data){
    return new Promise((resolve, reject) => {
        model.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao inserir escola: ${err}`);
            reject(err);
        })
    });
}

function updateEscola(data){
    return new Promise((resolve, reject) => {
        model.update(data, {
            where: { id:data.id } 
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`Erro ao editar escola: ${err}`);
            reject(err);
        })
    });
}

function deleteEscola(data){
    return new Promise((resolve, reject) => {
        model.destroy({where:{ id:data.id }}).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao excluir escola: ${err}`);
            reject(err);
        });
    });
}

module.exports = {buscarEscolas, insertEscola, updateEscola, deleteEscola};