const model = require('../models/estudanteModel');

function buscarEstudantes(skip, limit){
    return new Promise((resolve, reject) => {
        model.findAll({
            offset:skip,limit:limit
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao buscar estudante: ${err};`);
            reject(err);
        })
    }); 
}

function inserirEstudante(data){
    return new Promise((resolve, reject) => {
        model.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao inserir estudante: ${err};`);
            reject(err);
        })
    });
}

function updateEstudante(data){
    return new Promise((resolve, reject) => {
        model.update(data, {
            where: {id:data.id}
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao editar estudante: ${err};`);
            reject(err);
        })
    });
}

function deleteEstudante(data){
    return new Promise((resolve, reject) => {
        model.destroy({
            where: {id:data.id}
        }).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(`erro ao deletar estudante: ${err};`);
            reject(err);
        })
    });
}


module.exports = {buscarEstudantes, inserirEstudante, updateEstudante, deleteEstudante};