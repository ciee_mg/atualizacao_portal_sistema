const router = require('../config/server').server;
const service = require('../services/estagiario');
const validarToken = require('../config/token');
const funcao = require('../functions/functions');

router.post('/estagiarios', (request, response, next) => {
    if(validarToken(request.header('token'))){
        service.buscarEstagiarios(request.body.skip, request.body.limit).then(data => {
            response.send(200, funcao.retornos(true, 'busca realizada', data));
        }).catch(err => {
            response.send(503, err);
        })
    } else {
        response.send(403);
    }
    next();
});

router.post('/insereEstagiario', (request, response, next) => {
    service.insereEstagiario(request.body).then(data => {
        response.send(200, 'Inserção realizada com sucesso!')
    }).catch(err => {
        response.send(403)
    })
    next();
});

router.put('/updateEstagiario', (request, response, next) => {
    service.atualizaEstagiario(request.body).then(res => {
        response.send(200, 'Estagiário atualizado com sucesso')
    }).catch(err => {
        response.send(403)
    })
    next();
})

router.del('/deleteEstagiario', (request, response, next) => {
    service.deleteEstagiario(request.body).then(res => {
        response.send(200, 'Estagiario excluido com sucesso')
    }).catch(err => {
        response.send(400, err)
    })
    next();
})

router.get('/numEstagiarios', (request, response, next) => {
    service.numeroEstagiarios(request.body).then(res => {
        response.send(200, res)
    }).catch(err => {
        response.send(400, err)
    })
    next();
})

router.get('/teste', (request, response, next) => {
    service.nEst().then(data => {
        response.send(200, data);
    }).catch(err => {
        response.send(400, err)
    })
    next();
})
