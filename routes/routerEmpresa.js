const router = require('../config/server').server;
const service = require('../services/empresa');
const validarToken = require('../config/token');
const funcao = require('../functions/functions');

router.post('/empresa', (request, response, next) => {
    if(validarToken(request.header('token'))){
        service.buscarEmpresa(request.body.skip, request.body.limit).then(data => {
            response.send(200, funcao.retornos(true, 'busca realizada', data));
        }).catch(err => {
            response.send(503, err);
        })
    } else {
        response.send(403);
    }
    next();
});