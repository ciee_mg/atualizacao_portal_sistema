const router = require('../config/server').server;
const service = require('../services/estudante');
const validarToken = require('../config/token');
const funcao = require('../functions/functions');

router.post('/estudante', (request, response, next ) => {
    if(validarToken(request.header('token'))){
        service.buscarEstudantes(request.body.skip, request.body.limit).then(data =>{
            response.send(200, funcao.retornos(true, 'busca realizada', data));
        }).catch(err =>{
            response.send(503, err)
        })
    } else{
        response.send(403);
    }
    next();
});

router.post('/insereEstudante', (request, response, next) => {
    service.inserirEstudante(request.body).then(data => {
        response.send(200, 'Estudante inserido com sucesso');
    }).catch(err => {
        response.send(400, err)
    });
    next();
});

router.put('/updateEstudante', (request, response, next) => {
    service.updateEstudante(request.body).then(data => {
        response.send(200, 'Estudante Atualizado com Sucesso');
    }).catch(err => {
        response.send(400, err)
    });
    next();
});

router.del('/deleteEstudante',(request, response, next) => {
    service.deleteEstudante(request.body).then(data => {
        response.send(200, 'Estudante Deletado');
    }).catch(err => {
        response.send(400, err);
    });
    next();
});

module.exports = router;