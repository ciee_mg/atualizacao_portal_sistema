const escola = require('./routerEscolas');
const empresa = require('./routerEmpresa');
const estudante = require('./routerEstudante');
const estagiario = require('./routerEstagiario');

const router = [escola, empresa, estudante, estagiario];

module.exports = router;