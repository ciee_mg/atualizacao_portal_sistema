const router = require('../config/server').server;
const service = require('../services/escolas');
const validarToken = require('../config/token');
const funcao = require('../functions/functions');


router.post('/escolas', (request, response, next) => {
    if (validarToken(request.header('token'))){ 
        service.buscarEscolas(request.body.skip, request.body.limit).then(data => {
            response.send(200, funcao.retornos(true, 'busca realizada', data));
        }).catch(err => {
            response.send(503, err)
        })
    } else {
        response.send(403);
    }
    next(); 
});

router.post('/insereEscola', (request, response, next) => {
    service.insertEscola(request.body).then(data => {
        response.send(200, 'Escola inserida com sucesso!');
    }).catch(err => {
        response.send(400, err);
    })
    next();
});

router.put('/updateEscola', (request, response, next) => {
    service.updateEscola(request.body).then(data => {
        response.send(200, 'Escola atualizada com sucesso');
    }).catch(err => {
        response.send(400, err);
    })
    next();
});

router.del('/deleteEscola', (request, response, next) => {
    service.deleteEscola(request.body).then(data => {
        response.send(200, 'Escola excluida com sucesso');
    }).catch(err => {
        response.send(400, err);
    })
    next();
});

module.exports = router;