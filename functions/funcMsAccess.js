const dbAce = require('../config/db_msaccess');
 //11610
const estTotalGeral = "SELECT COUNT(*) FROM CIEE15 WHERE S141 = 1 AND TIPOPROGRAMA = '0';";

                    //   const estTotalGeral = "SELECT COUNT(*) FROM CIEE15 WHERE S141 = 1 AND (S27 <> '97' AND S27 <> '98' AND S27 <> '99' AND S27 <> '100' AND S27 <> '101' AND S27 <> '102'" 
                    //   +"  AND S27 <> '103' AND S27 <> '104' AND S27 <> '105' AND S27 <> '106'  AND S27 <> '107'  AND S27 <> '108'  AND S27 <> '110'  AND S27 <> '111'  AND S27 <> '112'"
                    //   +"  AND S27 <> '113'  AND S27 <> '114' AND S27 <> '115'  AND S27 <> '116'  AND S27 <> '117'  AND S27 <> '118'  AND S27 <> '119'  AND S27 <> '120'  AND S27 <> '121'"
                    //   +"  AND S27 <> '122' AND S27 <> '123'  AND S27 <> '124'  AND S27 <> '125');";                      

const aprendizGeral = "SELECT COUNT(*) FROM CIEE15 WHERE S141 = 1 AND TIPOPROGRAMA = '1';";

                    //  const aprendizGeral = "SELECT COUNT(*) FROM CIEE15 WHERE S141 = 1 AND (S27 = '101' OR S27 = '102' OR S27 = '103' OR S27 = '104' AND S27 = '105' OR S27 = '106'" 
                    //  +" OR S27 = '107' OR S27 = '108' OR S27 = '110' OR S27 = '111' OR S27 = '112' OR S27 = '113' OR S27 = '114' OR S27 = '115'  OR S27 = '116'"
                    //  +" OR S27 = '117' OR S27 = '118' OR S27 = '119' OR S27 = '120' OR S27 = '121' OR S27 = '122' OR S27 = '123' OR S27 = '124'  OR S27 = '125');"


// 0 - estagio 1 - aprendiz clt empresa 2 - aprendiz CLT CIEEMG
const aprendizCLT = "SELECT COUNT(*) FROM CIEE15 WHERE S141 = 1 AND TIPOPROGRAMA = '2';"


async function estagiariosGeral(){
    const estTotal = await dbAce.query(estTotalGeral);
    const aprTotal = await dbAce.query(aprendizGeral);
    const aprCltCIEE = await dbAce.query(aprendizCLT);

    const resolve = {
        totalEstagiarios: estTotal[0]["Expr1000"],
        totalAprendizesCltEmpresa:  aprTotal[0]["Expr1000"],
        totalAprendizesCltCIEEMG: aprCltCIEE[0]["Expr1000"]
    }
    
    return resolve;
    // return new Promise((resolve, reject) => {
    //     dbAce.query(estTotalGeral).then(res => {
    //         resolve(result = {
    //             totalEstagiarios: res[0].Expr1000
    //         })
    //     }).catch(err => {
    //         reject(err)
    //     })
    // })
}


module.exports = { estagiariosGeral };